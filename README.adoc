= EduGame

Education Project for school. Drag and drop items and try to create dice with specified number.

== Build and run

Requirements:

* python3
* pip

Install packages and run:

[source,bash]
----
pip install -r requirements.txt 
python3 main.py
----

== Future work

- [ ] Precistenie kodu 
- [ ] Spravit skutocne rotacie (a nie vytvaranie pre kazdu rotaciu novy objekt)
- [ ] Vyriesit prekryv obrazok na DropZone.


== CI/CD
Inspired by link:https://gitlab.com/gableroux/python-windows-application-example[gableroux/python-windows-application-example]

=== Linux
link:https://gitlab.com/Mishco/edu-game-dice/-/jobs/artifacts/master/download?job=build-linux[Latest build for Linux]

=== Windows
link:https://gitlab.com/Mishco/edu-game-dice/-/jobs/artifacts/master/download?job=build-windows[Latest build for Windows]
