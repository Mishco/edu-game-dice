"""
Edukacna hra pre skladanie kocky
"""
import os
import random
from tkinter import *
from tkinter import messagebox

import pygame
from pygame.sprite import Sprite, Group

# GLOBALS VARS
INVALID_MOVE = -3
COMIC_SANS_MS = "Comic Sans MS"
_image_library = {}


class State:
    def on_draw(self, surface): pass

    def on_event(self, event): pass

    def on_update(self, delta): pass


class Engine:
    @classmethod
    def setup(cls, caption, width, height, center=False):
        pygame.display.set_caption(caption)
        cls.surface = pygame.display.set_mode((width, height))
        cls.rect = cls.surface.get_rect()
        cls.clock = pygame.time.Clock()
        cls.running = False
        cls.delta = 0
        cls.fps = 30

        if center:
            os.environ['SDL_VIDEO_CENTERED'] = '1'

        cls.state = State()

    @classmethod
    def mainloop(cls):
        cls.running = True
        while cls.running:
            for event in pygame.event.get():
                cls.state.on_event(event)

            cls.state.on_update(cls.delta)
            cls.state.on_draw(cls.surface)
            pygame.display.flip()
            cls.delta = cls.clock.tick(cls.fps)


class SimpleSprite(Sprite):
    @classmethod
    def load_image(cls):
        cls.image = pygame.Surface((38, 38))
        cls.image.fill(pygame.Color('green'))

    def __init__(self, position, anchor="topleft"):
        Sprite.__init__(self)
        self.image = SimpleSprite.image
        self.rect = self.image.get_rect()
        setattr(self.rect, anchor, position)


def get_image(path):
    global _image_library
    image = _image_library.get(path)
    if image is None:
        canonically_path = path.replace('/', os.sep).replace('\\', os.sep)
        image = pygame.image.load(canonically_path)
        _image_library[path] = image
    return image


class OnePoint(Sprite):
    @classmethod
    def load_image(cls):
        cls.image = get_image('img/_1_.png')

    def __init__(self, position, anchor="topleft"):
        Sprite.__init__(self)
        self.image = OnePoint.image
        self.rect = self.image.get_rect()
        self.signature = 1
        setattr(self.rect, anchor, position)


class ZeroPoint(Sprite):
    @classmethod
    def load_image(cls):
        cls.image = get_image('img/_0_.png')

    def __init__(self, position, anchor="topleft"):
        Sprite.__init__(self)
        self.image = ZeroPoint.image
        self.rect = self.image.get_rect()
        self.signature = 0
        setattr(self.rect, anchor, position)


class DropZone(Sprite):
    @classmethod
    def load_image(cls):
        cls.image = pygame.Surface((80, 80))
        cls.image.fill(pygame.Color('grey'), cls.image.get_rect().inflate(-1, -1))

    def __init__(self, position, anchor="center", name='X', input_color='grey'):
        Sprite.__init__(self)
        # self.image = DropZone.image
        self.image = pygame.Surface((80, 80))
        self.image.fill(pygame.Color(input_color), self.image.get_rect().inflate(-1, -1))
        self.rect = self.image.get_rect()
        self.name_drop_zone = name
        self.act_value = -1  # default value,
        setattr(self.rect, anchor, position)


class PauseZone(Sprite):
    @classmethod
    def load_image(cls):
        # transparent surface
        cls.image = pygame.Surface((600, 1200), pygame.SRCALPHA, 32)
        cls.image.convert_alpha()

    def __init__(self, position, anchor="center"):
        Sprite.__init__(self)
        self.image = PauseZone.image
        self.rect = self.image.get_rect()
        self.act_value = -1
        setattr(self.rect, anchor, position)


# class RotationSign(Sprite):
#     @classmethod
#     def load_image(cls):
#         cls.image = get_image('img/rotate_sign.png')
#
#     def __init__(self, position, anchor="center"):
#         Sprite.__init__(self)
#         self.image = RotationSign.image
#         self.rect = self.image.get_rect()
#         setattr(self.rect, anchor, position)

class Row010(Sprite):
    @classmethod
    def load_image(cls):
        image0 = get_image('img/_0_.png')
        image1 = get_image('img/_1_.png')
        image2 = get_image('img/_0_.png')

        cls.image = pygame.Surface((image0.get_width() + image1.get_width() + image2.get_width(), image0.get_height()),
                                   pygame.SRCALPHA)
        cls.image.blit(image0, (0, 0))
        cls.image.blit(image1, (image0.get_width(), 0))
        cls.image.blit(image2, (image0.get_width() + image1.get_width(), 0))

    def __init__(self, position, anchor="center"):
        Sprite.__init__(self)
        self.image = Row010.image
        self.signature = [0, 1, 0]
        self.rect = self.image.get_rect()
        setattr(self.rect, anchor, position)


class Row010_r(Sprite):
    @classmethod
    def load_image(cls):
        image0 = get_image('img/_0_.png')
        image1 = get_image('img/_1_.png')
        image2 = get_image('img/_0_.png')

        cls.image = pygame.Surface(
            (image0.get_width(), image0.get_height() + image1.get_height() + image2.get_height()),
            pygame.SRCALPHA)
        cls.image.blit(image0, (0, 0))
        cls.image.blit(image1, (0, image0.get_height()))
        cls.image.blit(image2, (0, image0.get_height() + image1.get_height()))

    def __init__(self, position, anchor="center"):
        Sprite.__init__(self)
        self.image = Row010_r.image
        self.signature = [0, 1, 0]
        self.rect = self.image.get_rect()
        setattr(self.rect, anchor, position)


class Row010_r_A(Sprite):
    @classmethod
    def load_image(cls):
        image0 = get_image('img/_0_.png')
        image1 = get_image('img/_1_.png')
        image2 = get_image('img/_0_.png')

        cls.image = pygame.Surface((image0.get_width() + image1.get_width() + image2.get_width(), image0.get_height()),
                                   pygame.SRCALPHA)
        cls.image.blit(image0, (0, 0))
        cls.image.blit(image1, (image0.get_width(), 0))
        cls.image.blit(image2, (image0.get_width() + image1.get_width(), 0))

    def __init__(self, position, anchor="center"):
        Sprite.__init__(self)
        self.image = Row010_r_A.image
        self.signature = [0, 1, 0]
        self.rect = self.image.get_rect()
        setattr(self.rect, anchor, position)


class Row010_r_B(Sprite):
    @classmethod
    def load_image(cls):
        image0 = get_image('img/_0_.png')
        image1 = get_image('img/_1_.png')
        image2 = get_image('img/_0_.png')

        cls.image = pygame.Surface(
            (image0.get_width(), image0.get_height() + image1.get_height() + image2.get_height()),
            pygame.SRCALPHA)
        cls.image.blit(image0, (0, 0))
        cls.image.blit(image1, (0, image0.get_height()))
        cls.image.blit(image2, (0, image0.get_height() + image1.get_height()))

    def __init__(self, position, anchor="center"):
        Sprite.__init__(self)
        self.image = Row010_r_B.image
        self.signature = [0, 1, 0]
        self.rect = self.image.get_rect()
        setattr(self.rect, anchor, position)


class Row000(Sprite):
    @classmethod
    def load_image(cls):
        image0 = get_image('img/_0_.png')
        image1 = get_image('img/_0_.png')
        image2 = get_image('img/_0_.png')

        cls.image = pygame.Surface((image0.get_width() + image1.get_width() + image2.get_width(), image0.get_height()),
                                   pygame.SRCALPHA)
        cls.image.blit(image0, (0, 0))
        cls.image.blit(image1, (image0.get_width(), 0))
        cls.image.blit(image2, (image0.get_width() + image1.get_width(), 0))

    def __init__(self, position, anchor="center"):
        Sprite.__init__(self)
        self.image = Row000.image
        self.signature = [0, 0, 0]
        self.rect = self.image.get_rect()
        setattr(self.rect, anchor, position)


class Row000_r(Sprite):
    @classmethod
    def load_image(cls):
        image0 = get_image('img/_0_.png')
        image1 = get_image('img/_0_.png')
        image2 = get_image('img/_0_.png')

        cls.image = pygame.Surface(
            (image0.get_width(), image0.get_height() + image1.get_height() + image2.get_height()),
            pygame.SRCALPHA)
        cls.image.blit(image0, (0, 0))
        cls.image.blit(image1, (0, image0.get_height()))
        cls.image.blit(image2, (0, image0.get_height() + image1.get_height()))

    def __init__(self, position, anchor="center"):
        Sprite.__init__(self)
        self.image = Row000_r.image
        self.signature = [0, 0, 0]
        self.rect = self.image.get_rect()
        setattr(self.rect, anchor, position)


class Row000_r_A(Sprite):
    @classmethod
    def load_image(cls):
        image0 = get_image('img/_0_.png')
        image1 = get_image('img/_0_.png')
        image2 = get_image('img/_0_.png')

        cls.image = pygame.Surface((image0.get_width() + image1.get_width() + image2.get_width(), image0.get_height()),
                                   pygame.SRCALPHA)
        cls.image.blit(image0, (0, 0))
        cls.image.blit(image1, (image0.get_width(), 0))
        cls.image.blit(image2, (image0.get_width() + image1.get_width(), 0))

    def __init__(self, position, anchor="center"):
        Sprite.__init__(self)
        self.image = Row000_r_A.image
        self.signature = [0, 0, 0]
        self.rect = self.image.get_rect()
        setattr(self.rect, anchor, position)


class Row000_r_B(Sprite):
    @classmethod
    def load_image(cls):
        image0 = get_image('img/_0_.png')
        image1 = get_image('img/_0_.png')
        image2 = get_image('img/_0_.png')

        cls.image = pygame.Surface(
            (image0.get_width(), image0.get_height() + image1.get_height() + image2.get_height()),
            pygame.SRCALPHA)
        cls.image.blit(image0, (0, 0))
        cls.image.blit(image1, (0, image0.get_height()))
        cls.image.blit(image2, (0, image0.get_height() + image1.get_height()))

    def __init__(self, position, anchor="center"):
        Sprite.__init__(self)
        self.image = Row000_r_B.image
        self.signature = [0, 0, 0]
        self.rect = self.image.get_rect()
        setattr(self.rect, anchor, position)


class Row101(Sprite):
    @classmethod
    def load_image(cls):
        image0 = get_image('img/_1_.png')
        image1 = get_image('img/_0_.png')
        image2 = get_image('img/_1_.png')

        cls.image = pygame.Surface((image0.get_width() + image1.get_width() + image2.get_width(), image0.get_height()),
                                   pygame.SRCALPHA)
        cls.image.blit(image0, (0, 0))
        cls.image.blit(image1, (image0.get_width(), 0))
        cls.image.blit(image2, (image0.get_width() + image1.get_width(), 0))

    def __init__(self, position, anchor="center"):
        Sprite.__init__(self)
        self.image = Row101.image
        self.signature = [1, 0, 1]
        self.rect = self.image.get_rect()
        setattr(self.rect, anchor, position)


class Row101_r(Sprite):
    @classmethod
    def load_image(cls):
        image0 = get_image('img/_1_.png')
        image1 = get_image('img/_0_.png')
        image2 = get_image('img/_1_.png')

        cls.image = pygame.Surface(
            (image0.get_width(), image0.get_height() + image1.get_height() + image2.get_height()),
            pygame.SRCALPHA)
        cls.image.blit(image0, (0, 0))
        cls.image.blit(image1, (0, image0.get_height()))
        cls.image.blit(image2, (0, image0.get_height() + image1.get_height()))

    def __init__(self, position, anchor="center"):
        Sprite.__init__(self)
        self.image = Row101_r.image
        self.signature = [1, 0, 1]
        self.rect = self.image.get_rect()
        setattr(self.rect, anchor, position)


class Row101_r_A(Sprite):
    @classmethod
    def load_image(cls):
        image0 = get_image('img/_1_.png')
        image1 = get_image('img/_0_.png')
        image2 = get_image('img/_1_.png')

        cls.image = pygame.Surface((image0.get_width() + image1.get_width() + image2.get_width(), image0.get_height()),
                                   pygame.SRCALPHA)
        cls.image.blit(image0, (0, 0))
        cls.image.blit(image1, (image0.get_width(), 0))
        cls.image.blit(image2, (image0.get_width() + image1.get_width(), 0))

    def __init__(self, position, anchor="center"):
        Sprite.__init__(self)
        self.image = Row101_r_A.image
        self.signature = [1, 0, 1]
        self.rect = self.image.get_rect()
        setattr(self.rect, anchor, position)


class Row101_r_B(Sprite):
    @classmethod
    def load_image(cls):
        image0 = get_image('img/_1_.png')
        image1 = get_image('img/_0_.png')
        image2 = get_image('img/_1_.png')

        cls.image = pygame.Surface(
            (image0.get_width(), image0.get_height() + image1.get_height() + image2.get_height()),
            pygame.SRCALPHA)
        cls.image.blit(image0, (0, 0))
        cls.image.blit(image1, (0, image0.get_height()))
        cls.image.blit(image2, (0, image0.get_height() + image1.get_height()))

    def __init__(self, position, anchor="center"):
        Sprite.__init__(self)
        self.image = Row101_r_B.image
        self.signature = [1, 0, 1]
        self.rect = self.image.get_rect()
        setattr(self.rect, anchor, position)


class Row100(Sprite):
    @classmethod
    def load_image(cls):
        image0 = get_image('img/_1_.png')
        image1 = get_image('img/_0_.png')
        image2 = get_image('img/_0_.png')

        cls.image = pygame.Surface((image0.get_width() + image1.get_width() + image2.get_width(), image0.get_height()),
                                   pygame.SRCALPHA)
        cls.image.blit(image0, (0, 0))
        cls.image.blit(image1, (image0.get_width(), 0))
        cls.image.blit(image2, (image0.get_width() + image1.get_width(), 0))

    def __init__(self, position, anchor="center"):
        Sprite.__init__(self)
        self.image = Row100.image
        self.signature = [1, 0, 0]
        self.rect = self.image.get_rect()
        setattr(self.rect, anchor, position)


class Row100_r(Sprite):
    @classmethod
    def load_image(cls):
        image0 = get_image('img/_1_.png')
        image1 = get_image('img/_0_.png')
        image2 = get_image('img/_0_.png')

        cls.image = pygame.Surface(
            (image0.get_width(), image0.get_height() + image1.get_height() + image2.get_height()),
            pygame.SRCALPHA)
        cls.image.blit(image0, (0, 0))
        cls.image.blit(image1, (0, image0.get_height()))
        cls.image.blit(image2, (0, image0.get_height() + image1.get_height()))

    def __init__(self, position, anchor="center"):
        Sprite.__init__(self)
        self.image = Row100_r.image
        self.signature = [1, 0, 0]
        self.rect = self.image.get_rect()
        setattr(self.rect, anchor, position)


class Row100_r_A(Sprite):
    @classmethod
    def load_image(cls):
        image0 = get_image('img/_0_.png')
        image1 = get_image('img/_0_.png')
        image2 = get_image('img/_1_.png')

        cls.image = pygame.Surface((image0.get_width() + image1.get_width() + image2.get_width(), image0.get_height()),
                                   pygame.SRCALPHA)
        cls.image.blit(image0, (0, 0))
        cls.image.blit(image1, (image0.get_width(), 0))
        cls.image.blit(image2, (image0.get_width() + image1.get_width(), 0))

    def __init__(self, position, anchor="center"):
        Sprite.__init__(self)
        self.image = Row100_r_A.image
        self.signature = [0, 0, 1]
        self.rect = self.image.get_rect()
        setattr(self.rect, anchor, position)


class Row100_r_B(Sprite):
    @classmethod
    def load_image(cls):
        image0 = get_image('img/_0_.png')
        image1 = get_image('img/_0_.png')
        image2 = get_image('img/_1_.png')

        cls.image = pygame.Surface(
            (image0.get_width(), image0.get_height() + image1.get_height() + image2.get_height()),
            pygame.SRCALPHA)
        cls.image.blit(image0, (0, 0))
        cls.image.blit(image1, (0, image0.get_height()))
        cls.image.blit(image2, (0, image0.get_height() + image1.get_height()))

    def __init__(self, position, anchor="center"):
        Sprite.__init__(self)
        self.image = Row100_r_B.image
        self.signature = [0, 0, 1]
        self.rect = self.image.get_rect()
        setattr(self.rect, anchor, position)


class MouseGrab:
    """
    All mouse events, drag, drop, move, rotate
    """

    def __init__(self):
        self.selected = None
        self.home_rect = None
        self.grab_position = None
        self.clockwise = True

    def grab(self, pos, sprites):
        for sprite in sprites:
            # skip drag-and-drop for dropzone
            if sprite.__class__ == DropZone or sprite.__class__ == Button or sprite.__class__ == PauseZone:
                continue
            if sprite.rect.collidepoint(pos):
                self.grab_position = (pos[0] - sprite.rect.x,
                                      pos[1] - sprite.rect.y)
                self.home_rect = sprite.rect.copy()
                self.selected = sprite
                return

    def drop(self, pos, grid, sprites):
        if self.selected:
            for col in grid:
                for rect in col:
                    if rect.collidepoint(pos):
                        self.selected.rect.center = rect.center
                        for sprite in sprites:
                            if sprite.rect.collidepoint(
                                    pos) and sprite.__class__ == DropZone and sprite.__class__ == PauseZone \
                                    and self.selected.__class__ != Row010 \
                                    and self.selected.__class__ == Row010_r and self.selected.__class__ == Row010_r_A \
                                    and self.selected.__class__ == Row010_r_B \
                                    and self.selected.__class__ != Row000 and self.selected.__class__ != Row000_r \
                                    and self.selected.__class__ != Row000_r_A and self.selected.__class__ != Row000_r_B \
                                    and self.selected.__class__ != Row101 \
                                    and self.selected.__class__ != Row101_r and self.selected.__class__ != Row101_r_A \
                                    and self.selected.__class__ != Row101_r_B \
                                    and self.selected.__class__ != Row100 and self.selected.__class__ != Row100_r \
                                    and self.selected.__class__ != Row100_r_A and self.selected.__class__ != Row100_r_B:
                                sprite.act_value = self.selected.signature
                                self.selected = None
                                return
                            if sprite.rect.collidepoint(
                                    pos) and sprite.__class__ == DropZone \
                                    and (self.selected.__class__ == Row010
                                         or self.selected.__class__ == Row000
                                         or self.selected.__class__ == Row101
                                         or self.selected.__class__ == Row100
                                         or self.selected.__class__ == Row100_r_A
                                         or self.selected.__class__ == Row101_r_A
                                         or self.selected.__class__ == Row010_r_A
                                         or self.selected.__class__ == Row000_r_A):

                                collide = pygame.sprite.spritecollide(self.selected, sprites, False,
                                                                      pygame.sprite.collide_rect)
                                print("Pokryvam: {}".format(len(collide)))
                                if len(collide) == 5:
                                    # prekryvaju sa dve rozne kocky:
                                    print("Zly prekryv")
                                    messagebox.showinfo('Zly prekryv', 'Kocky sa nemôžu prekrývať, skús znova')
                                    # self.reset_game()
                                    return INVALID_MOVE
                                elif len(collide) == 4:
                                    # musi byt polozena cela
                                    collide_dict = {}
                                    for cld in collide:
                                        if cld.__class__ == DropZone:
                                            collide_dict[cld.name_drop_zone] = cld

                                    for cld in collide:
                                        # v prvom riadku
                                        if cld.__class__ == DropZone:
                                            if cld.name_drop_zone == 'A' or cld.name_drop_zone == 'B' or cld.name_drop_zone == 'C':
                                                collide_dict['A'].act_value = self.selected.signature[0]  # 0
                                                collide_dict['B'].act_value = self.selected.signature[1]  # 1
                                                collide_dict['C'].act_value = self.selected.signature[2]  # 0
                                            if cld.name_drop_zone == 'D' or cld.name_drop_zone == 'E' or cld.name_drop_zone == 'F':
                                                collide_dict['D'].act_value = self.selected.signature[0]
                                                collide_dict['E'].act_value = self.selected.signature[1]
                                                collide_dict['F'].act_value = self.selected.signature[2]
                                            if cld.name_drop_zone == 'G' or cld.name_drop_zone == 'H' or cld.name_drop_zone == 'I':
                                                collide_dict['G'].act_value = self.selected.signature[0]
                                                collide_dict['H'].act_value = self.selected.signature[1]
                                                collide_dict['I'].act_value = self.selected.signature[2]

                                    self.selected = None
                                    return
                                else:
                                    return

                            if sprite.rect.collidepoint(pos) and sprite.__class__ == DropZone \
                                    and (self.selected.__class__ == Row100_r or self.selected.__class__ == Row100_r_B
                                         or self.selected.__class__ == Row101_r or self.selected.__class__ == Row101_r_B
                                         or self.selected.__class__ == Row010_r or self.selected.__class__ == Row010_r_B
                                         or self.selected.__class__ == Row000_r or self.selected.__class__ == Row000_r_B):

                                collide = pygame.sprite.spritecollide(self.selected, sprites, False,
                                                                      pygame.sprite.collide_rect)
                                print("Pokryvam: {} stvorceky".format(len(collide)))
                                if len(collide) == 5:
                                    # prekryvaju sa dve rozne kocky:
                                    print("Zly prekryv")
                                    messagebox.showinfo('Zly prekryv', 'Kocky sa nemôžu prekrývať, skús znova')
                                    return
                                elif len(collide) == 4:
                                    collide_dict = {}
                                    for cld in collide:
                                        if cld.__class__ == DropZone:
                                            collide_dict[cld.name_drop_zone] = cld

                                    for cld in collide:
                                        # v prvom stlpci
                                        if cld.__class__ == DropZone:
                                            if cld.name_drop_zone == 'A' or cld.name_drop_zone == 'D' or cld.name_drop_zone == 'G':
                                                collide_dict['A'].act_value = self.selected.signature[0]  # 0
                                                collide_dict['D'].act_value = self.selected.signature[1]  # 1
                                                collide_dict['G'].act_value = self.selected.signature[2]  # 0
                                            if cld.name_drop_zone == 'B' or cld.name_drop_zone == 'E' or cld.name_drop_zone == 'H':
                                                collide_dict['B'].act_value = self.selected.signature[0]
                                                collide_dict['E'].act_value = self.selected.signature[1]
                                                collide_dict['H'].act_value = self.selected.signature[2]
                                            if cld.name_drop_zone == 'C' or cld.name_drop_zone == 'F' or cld.name_drop_zone == 'I':
                                                collide_dict['C'].act_value = self.selected.signature[0]
                                                collide_dict['F'].act_value = self.selected.signature[1]
                                                collide_dict['I'].act_value = self.selected.signature[2]

                                    self.selected = None
                                    return
                                else:
                                    return

                            if sprite.rect.collidepoint(pos) and sprite.__class__ == PauseZone:
                                self.selected = None
                                return

    def move(self, pos, sprites):
        if self.selected:
            x = pos[0] - self.grab_position[0]
            y = pos[1] - self.grab_position[1]
            self.selected.rect.topleft = x, y

    def rotate(self, sprites, event):
        if self.selected is None:
            return

        self.selected.rect.center = pygame.mouse.get_pos()
        print("rotujem {}".format(self.clockwise))
        # ak mam Row100 a clockwise idem na Row_rotated
        # ak mam Row_rotated AND clockwise idem na row_rotated_a
        # ak mam row_rotated_a AND clockwise idem na row_rotated_b
        # ak mam row_rotated_b AND clockwise idem na Row100

        if self.selected.__class__ == Row100 and self.clockwise:
            row_100_rotated = Row100_r(self.selected.rect.center, 'center')
            sprites.add(row_100_rotated)
            sprites.remove(self.selected)
            self.selected = None
            self.selected = row_100_rotated
            return
        if self.selected.__class__ == Row100_r and self.clockwise:
            row_100_a = Row100_r_A(self.selected.rect.center, 'center')
            sprites.add(row_100_a)
            sprites.remove(self.selected)
            self.selected = None
            self.selected = row_100_a
            return
        if self.selected.__class__ == Row100_r_A and self.clockwise:
            row_100_b = Row100_r_B(self.selected.rect.center, 'center')
            sprites.add(row_100_b)
            sprites.remove(self.selected)
            self.selected = None
            self.selected = row_100_b
            return
        if self.selected.__class__ == Row100_r_B and self.clockwise:
            row_100 = Row100(self.selected.rect.center, 'center')
            sprites.add(row_100)
            sprites.remove(self.selected)
            self.selected = None
            self.selected = row_100
            return

        if self.selected.__class__ == Row101 and self.clockwise:
            row_101_rotated = Row101_r(self.selected.rect.center, 'center')
            sprites.add(row_101_rotated)
            sprites.remove(self.selected)
            self.selected = None
            self.selected = row_101_rotated
            return
        if self.selected.__class__ == Row101_r and self.clockwise:
            row_101_a = Row101_r_A(self.selected.rect.center, 'center')
            sprites.add(row_101_a)
            sprites.remove(self.selected)
            self.selected = None
            self.selected = row_101_a
            return
        if self.selected.__class__ == Row101_r_A and self.clockwise:
            row_101_b = Row101_r_B(self.selected.rect.center, 'center')
            sprites.add(row_101_b)
            sprites.remove(self.selected)
            self.selected = None
            self.selected = row_101_b
            return
        if self.selected.__class__ == Row101_r_B and self.clockwise:
            row_101 = Row101(self.selected.rect.center, 'center')
            sprites.add(row_101)
            sprites.remove(self.selected)
            self.selected = None
            self.selected = row_101
            return

        if self.selected.__class__ == Row010 and self.clockwise:
            row_010_rotated = Row010_r(self.selected.rect.center, 'center')
            sprites.add(row_010_rotated)
            sprites.remove(self.selected)
            self.selected = None
            self.selected = row_010_rotated
            return
        if self.selected.__class__ == Row010_r and self.clockwise:
            row_010_r_A = Row010_r_A(self.selected.rect.center, 'center')
            sprites.add(row_010_r_A)
            sprites.remove(self.selected)
            self.selected = None
            self.selected = row_010_r_A
            return
        if self.selected.__class__ == Row010_r_A and self.clockwise:
            row_010_b = Row010_r_B(self.selected.rect.center, 'center')
            sprites.add(row_010_b)
            sprites.remove(self.selected)
            self.selected = None
            self.selected = row_010_b
            return
        if self.selected.__class__ == Row010_r_B and self.clockwise:
            row_010 = Row010(self.selected.rect.center, 'center')
            sprites.add(row_010)
            sprites.remove(self.selected)
            self.selected = None
            self.selected = row_010
            return

        if self.selected.__class__ == Row000 and self.clockwise:
            row_000_rotated = Row000_r(self.selected.rect.center, 'center')
            sprites.add(row_000_rotated)
            sprites.remove(self.selected)
            self.selected = None
            self.selected = row_000_rotated
            return
        if self.selected.__class__ == Row000_r and self.clockwise:
            row_000_r_A = Row000_r_A(self.selected.rect.center, 'center')
            sprites.add(row_000_r_A)
            sprites.remove(self.selected)
            self.selected = None
            self.selected = row_000_r_A
            return
        if self.selected.__class__ == Row000_r_A and self.clockwise:
            row_000_b = Row000_r_B(self.selected.rect.center, 'center')
            sprites.add(row_000_b)
            sprites.remove(self.selected)
            self.selected = None
            self.selected = row_000_b
            return
        if self.selected.__class__ == Row000_r_B and self.clockwise:
            row_000 = Row000(self.selected.rect.center, 'center')
            sprites.add(row_000)
            sprites.remove(self.selected)
            self.selected = None
            self.selected = row_000
            return


class Button(Sprite):
    """
    Button is a sprite subclass, that means it can be added to a sprite group.
    You can draw and update all sprites in a group by
    calling `group.update()` and `group.draw(screen)`.
    """

    def __init__(self, x, y, width, height, callback,
                 font, image_normal, image_hover,
                 image_down, text='', text_color=(0, 0, 0)):
        super().__init__()
        # Scale the images to the desired size (doesn't modify the originals).
        self.image_normal = pygame.transform.scale(image_normal, (width, height))
        self.image_hover = pygame.transform.scale(image_hover, (width, height))
        self.image_down = pygame.transform.scale(image_down, (width, height))

        self.image = self.image_normal  # The currently active image.
        self.rect = self.image.get_rect(topleft=(x, y))
        # To center the text rect.
        image_center = self.image.get_rect().center
        text_surf = font.render(text, True, text_color)
        text_rect = text_surf.get_rect(center=image_center)
        # Blit the text onto the images.
        for image in (self.image_normal, self.image_hover, self.image_down):
            image.blit(text_surf, text_rect)

        # This function will be called when the button gets pressed.
        self.callback = callback
        self.button_down = False

    def handle_event(self, event):
        if event.type == pygame.MOUSEBUTTONDOWN:
            if self.rect.collidepoint(event.pos):
                self.image = self.image_down
                self.button_down = True
        elif event.type == pygame.MOUSEBUTTONUP:
            # If the rect collides with the mouse pos.
            if self.rect.collidepoint(event.pos) and self.button_down:
                self.callback()  # Call the function.
                self.image = self.image_hover
            self.button_down = False
        elif event.type == pygame.MOUSEMOTION:
            collided = self.rect.collidepoint(event.pos)
            if collided and not self.button_down:
                self.image = self.image_hover
            elif not collided:
                self.image = self.image_normal


class Text(Sprite):
    def __init__(self, text, size, color, width, height):
        Sprite.__init__(self)
        self.font = pygame.font.SysFont(COMIC_SANS_MS, size)
        self.textSurf = self.font.render(text, 1, color)
        self.image = pygame.Surface((width, height))
        self.image.fill(pygame.Color('steelblue'))

        W = self.textSurf.get_width()
        H = self.textSurf.get_height()
        self.image.blit(self.textSurf, [width / 2 - W / 2, height / 2 - H / 2])


class MatrixResults:
    def find(self, num):
        """Dispatch method"""
        method_name = 'matrix_' + str(num)
        # Get the method from 'self'. Default to a lambda.
        method = getattr(self, method_name, lambda: "Invalid matrix number")
        # Call the method as we return it
        return method()

    @classmethod
    def matrix_1(cls):
        return [[0, 0, 0],
                [0, 1, 0],
                [0, 0, 0]]

    @classmethod
    def matrix_2(cls):
        return [[1, 0, 0],
                [0, 0, 0],
                [0, 0, 1]]

    @classmethod
    def matrix_3(cls):
        return [[1, 0, 0],
                [0, 1, 0],
                [0, 0, 1]]

    @classmethod
    def matrix_4(cls):
        return [[1, 0, 1],
                [0, 0, 0],
                [1, 0, 1]]

    @classmethod
    def matrix_5(cls):
        return [[1, 0, 1],
                [0, 1, 0],
                [1, 0, 1]]

    @classmethod
    def matrix_6(cls):
        return [[1, 0, 1],
                [1, 0, 1],
                [1, 0, 1]]


class Edugame(State):
    def __init__(self):
        self.grid = []
        self.sprites = Group()
        self.buttons = Group()
        self.texts = Group()
        self.all_drop_zone = Group()
        self.mouse = MouseGrab()

        FONT = pygame.font.SysFont(COMIC_SANS_MS, 32)
        IMAGE_NORMAL = pygame.Surface((100, 32))
        IMAGE_NORMAL.fill(pygame.Color('dodgerblue1'))
        IMAGE_HOVER = pygame.Surface((100, 32))
        IMAGE_HOVER.fill(pygame.Color('lightskyblue'))
        IMAGE_DOWN = pygame.Surface((100, 32))
        IMAGE_DOWN.fill(pygame.Color('aquamarine1'))

        # grid_size = 40, 40
        grid_size = 80, 80
        for x in range(0, Engine.rect.width, grid_size[0]):
            col = []
            for y in range(0, Engine.rect.height, grid_size[1]):
                col.append(pygame.Rect(x, y, grid_size[0], grid_size[1]))

            self.grid.append(col)

        starting_point = 920  # 440
        starting_point_y = 520  # 200
        drop_zoneA = DropZone((starting_point, starting_point_y), 'center', 'A')
        drop_zoneA.add(self.sprites)
        drop_zoneB = DropZone((starting_point + 80, starting_point_y), 'center', 'B', 'orange')
        drop_zoneB.add(self.sprites)
        drop_zoneC = DropZone((starting_point + 2 * 80, starting_point_y), 'center', 'C')
        drop_zoneC.add(self.sprites)
        drop_zoneD = DropZone((starting_point, starting_point_y + 80), 'center', 'D', 'orange')
        drop_zoneD.add(self.sprites)
        drop_zoneE = DropZone((starting_point + 80, starting_point_y + 80), 'center', 'E', 'orange')
        drop_zoneE.add(self.sprites)
        drop_zoneF = DropZone((starting_point + 2 * 80, starting_point_y + 80), 'center', 'F', 'orange')
        drop_zoneF.add(self.sprites)
        drop_zoneG = DropZone((starting_point, starting_point_y + 2 * 80), 'center', 'G')
        drop_zoneG.add(self.sprites)
        drop_zoneH = DropZone((starting_point + 80, starting_point_y + 2 * 80), 'center', 'H', 'orange')
        drop_zoneH.add(self.sprites)
        drop_zoneI = DropZone((starting_point + 2 * 80, starting_point_y + 2 * 80), 'center', 'I')
        drop_zoneI.add(self.sprites)

        self.all_drop_zone.add(drop_zoneA, drop_zoneB, drop_zoneC, drop_zoneD, drop_zoneE, drop_zoneF, drop_zoneG,
                               drop_zoneH, drop_zoneI)

        # FIXME
        # rs = RotationSign((400,400),'center')
        # rs.add(self.sprites)

        pause_zone = PauseZone((100, 400), 'center')
        pause_zone.add(self.sprites)

        for i in range(10):
            # one_point = OnePoint((100, 100), 'center')
            # one_point.add(self.sprites)
            # zero_point = ZeroPoint((100, 180), 'center')
            # zero_point.add(self.sprites)
            row_010 = Row010((100, 260), 'center')
            row_010.add(self.sprites)
            row_000 = Row000((100, 340), 'center')
            row_000.add(self.sprites)
            row_101 = Row101((100, 420), 'center')
            row_101.add(self.sprites)
            row_100 = Row100((100, 500), 'center')
            row_100.add(self.sprites)

        self.number = random.randint(1, 6)

        text = Text("Hodil si kockou: {}".format(self.number), 32, pygame.Color('gray99'), 420, 120)
        text.rect = (400, 120)
        self.texts.add(text)

        self.check_button = Button(
            920, 360, 170, 65, self.check_dice,
            FONT, IMAGE_NORMAL, IMAGE_HOVER, IMAGE_DOWN, 'Skontroluj', (255, 255, 255))

        self.reset_button = Button(
            920, 160, 170, 65, self.reset_game,
            FONT, IMAGE_NORMAL, IMAGE_HOVER, IMAGE_DOWN, 'Reset', (255, 0, 0))

        self.buttons.add(self.check_button, self.reset_button)

        # to hide the main window
        Tk().wm_withdraw()

    def check_dice(self):
        print("Kontrolujem...")
        drop_zone = self.all_drop_zone.sprites()
        self.all_drop_zone.sprites()

        w, h = 3, 3
        matrix = [[0 for x in range(w)] for y in range(h)]
        matrix[0][0] = drop_zone[0].act_value
        matrix[0][1] = drop_zone[1].act_value
        matrix[0][2] = drop_zone[2].act_value

        matrix[1][0] = drop_zone[3].act_value
        matrix[1][1] = drop_zone[4].act_value
        matrix[1][2] = drop_zone[5].act_value

        matrix[2][0] = drop_zone[6].act_value
        matrix[2][1] = drop_zone[7].act_value
        matrix[2][2] = drop_zone[8].act_value

        try:
            print('\n'.join([''.join(['{:4}'.format(item) for item in row])
                             for row in matrix]))

            mr = MatrixResults()
            for x in range(1, 7):
                if self.number == x:
                    if matrix == mr.find(x):
                        print("Spravne")
                        messagebox.showinfo('Vysledok', 'Spravne')
                        self.reset_game()
                    else:
                        print("Nespravne")
                        messagebox.showwarning('Vysledok', 'Nespravne')
                        self.reset_game()
        except:
            print("> Matica ma nespravne hodnoty a nemoze byt skontrolovana! <")

    def on_draw(self, surface):
        surface.fill(pygame.Color('steelblue'))
        self.sprites.draw(surface)
        self.buttons.draw(surface)
        self.texts.draw(surface)

    def on_event(self, event):
        if event.type == pygame.QUIT:
            Engine.running = False
        elif event.type == pygame.MOUSEBUTTONDOWN:
            if event.button == 1:
                self.mouse.grab(event.pos, self.sprites)
        elif event.type == pygame.MOUSEBUTTONUP:
            if event.button == 1:
                res = self.mouse.drop(event.pos, self.grid, self.sprites)
                if res == INVALID_MOVE:
                    self.reset_game()
        elif event.type == pygame.MOUSEMOTION:
            self.mouse.move(event.pos, self.sprites)
        elif event.type == pygame.KEYDOWN:
            self.mouse.rotate(self.sprites, event)

        for button in self.buttons:
            button.handle_event(event)

    def reset_game(self):
        main()


def main():
    """
    Hlavna methoda programu
    nacitanie vsetkych obrazkov a objektov sa vykona iba raz.
    Posledny krok je spustenie hlavneho cyklu.
    :return: Nothing
    """
    pygame.init()
    Engine.setup("EdukacnaHra", 1200, 800, True)

    SimpleSprite.load_image()
    OnePoint.load_image()
    ZeroPoint.load_image()
    DropZone.load_image()
    PauseZone.load_image()

    Row010.load_image()
    Row010_r.load_image()
    Row010_r_A.load_image()
    Row010_r_B.load_image()

    Row000.load_image()
    Row000_r.load_image()
    Row000_r_A.load_image()
    Row000_r_B.load_image()

    Row101.load_image()
    Row101_r.load_image()
    Row101_r_A.load_image()
    Row101_r_B.load_image()

    Row100.load_image()
    Row100_r.load_image()
    Row100_r_A.load_image()
    Row100_r_B.load_image()

    # RotationSign.load_image()

    Engine.state = Edugame()
    Engine.mainloop()


if __name__ == '__main__':
    main()
